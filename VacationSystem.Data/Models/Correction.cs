﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace VacationSystem.Data.Models
{
    public class Correction
    {
        public int Id { get; set; }
        public DateTime CorrectionDate { get; set; }
        public string Reason { get; set; }

        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }

        public int RequestId { get; set; }
        public Request Request { get; set; }
    }
    public class CorrectionConfiguration : IEntityTypeConfiguration<Correction>
    {
        public void Configure(EntityTypeBuilder<Correction> builder)
        {
            builder.Property(x => x.Reason).IsRequired();
            builder.Property(x => x.CorrectionDate).IsRequired();
            builder.Property(x => x.EmployeeId).IsRequired();
        }
    }
}
