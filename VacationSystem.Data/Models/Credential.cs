﻿using System.ComponentModel.DataAnnotations.Schema;

namespace VacationSystem.Data.Models
{
    public class Credential
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }

        [NotMapped]
        public string RepeatPassword { get; set; }
        public string Password { get; set; }
        public byte[] Salt { get; set; }
    }
}
