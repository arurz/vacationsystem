﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace VacationSystem.Data.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }

        public int RoleId { get; set; }
        public Role Role { get; set; }

      
        public string Position { get; set; }
        public int FurloughLeft { get; set; }

        public Credential Credential { get; set; }
    }

    public abstract class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.Property(x => x.Firstname).IsRequired();
            builder.Property(x => x.Lastname).IsRequired();
            builder.Property(x => x.Position).IsRequired();

            builder.HasOne(c => c.Credential)
            .WithOne(e => e.Employee)
            .HasForeignKey<Credential>(c => c.Id);
        }
    }
}
