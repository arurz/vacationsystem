﻿using System;
using System.Collections.Generic;
using VacationSystem.Data.Enums;

namespace VacationSystem.Data.Models
{
    public class Request
    {
        public int Id { get; set; }

        public DateTime RequestDate { get; set; }
        public int Year { get; set; }
        public int NumberOfDays { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public Status Status { get; set; }

        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }

        public ICollection<Correction> Corrections { get; set; }

    }
}
