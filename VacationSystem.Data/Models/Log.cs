﻿using System;

namespace VacationSystem.Data.Models
{
    public class Log
    {
        public int Id { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public string Verb { get; set; }
        public string Url { get; set; }
        public DateTime AppearenceDateTime { get; set; }
        public int? UserId { get; set; }
    }
}
