﻿namespace VacationSystem.Data.Enums
{
    public enum Status
    {
        Pending,
        Approved,
        AwaitingCorrection,
        Declined
    }
}
