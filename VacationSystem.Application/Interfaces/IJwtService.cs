﻿using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Data.Models;

namespace VacationSystem.Application.Interfaces
{
    public interface IJwtService
    {
        Task<string> GenerateJwt(Employee person, CancellationToken cancellationToken);
    }
}
