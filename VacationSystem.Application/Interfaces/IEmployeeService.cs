﻿using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Models;

namespace VacationSystem.Application.Interfaces
{
    public interface IEmployeeService
    {
        Task<EmployeeDto> GetEmployeeById(int id, CancellationToken cancellationToken);
    }
}
