﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace VacationSystem.Application.Interfaces
{
    public interface ILoggingService
    {
        Task LogRequest(HttpContext httpContext);

        Task LogException(Exception exception, HttpContext httpContext);
    }
}
