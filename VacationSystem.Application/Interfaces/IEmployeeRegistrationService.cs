﻿using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Data.Models;

namespace VacationSystem.Application.Interfaces
{
    public interface IEmployeeRegistrationService
    {
        Task RegisterEmployee(Employee employee, CancellationToken cancellationToken);
    }
}
