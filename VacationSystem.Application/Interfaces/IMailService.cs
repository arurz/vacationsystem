﻿using System.Threading;
using System.Threading.Tasks;

namespace VacationSystem.Application.Interfaces
{
    public interface IMailService
    {
        Task SendMail(int employeeId, string correctionDetails, string status, CancellationToken cancellationToken);
    }
}
