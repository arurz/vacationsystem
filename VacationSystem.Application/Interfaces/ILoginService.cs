﻿using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Models;
using VacationSystem.Data.Models;

namespace VacationSystem.Application.Interfaces
{
    public interface ILoginService
    {
        Task<Employee> LoginAsync(LoginDto model, CancellationToken cancellationToken);
    }
}
