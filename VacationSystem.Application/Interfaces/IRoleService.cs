﻿using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Data.Models;

namespace VacationSystem.Application.Interfaces
{
    public interface IRoleService
    {
        Task CreateRole(Role role, CancellationToken cancellationToken);
        Task DeleteRole(int id, CancellationToken cancellationToken);
    }
}
