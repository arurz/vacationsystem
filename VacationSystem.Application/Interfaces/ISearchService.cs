﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Models;

namespace VacationSystem.Application.Interfaces
{
    public interface ISearchService
    {
        Task<List<RequestDto>> SearchRequests(SearchDto searchDto, CancellationToken cancellationToken);
    }
}
