﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Data.Models;

namespace VacationSystem.Application.Interfaces
{
    public interface IRequestService
    {
        Task<Request> CreateRequest(Request request, CancellationToken cancellationToken);

        Task DeleteRequest(int id, CancellationToken cancellationToken);

        Task<List<Request>> GetAllRequests(CancellationToken cancellationToken);

        Task<Request> GetRequestById(int id, CancellationToken cancellationToken);

        Task<int> GetMaxVacationDays(CancellationToken cancellationToken);

        Task<Request> UpdateRequest(Request request, CancellationToken cancellationToken);
    }
}
