﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Models;
using VacationSystem.Data.Enums;
using VacationSystem.Data.Models;

namespace VacationSystem.Application.Interfaces
{
    public interface IAdminService
    {
        Task<Request> ChangeRequestStatus(int id, Status status, int userId, CancellationToken cancellationToken);

        Task<Request> CreateCorrectionForRequest(Correction correction, CancellationToken cancellationToken);

        Task<int> GetMaxFurloughLeft(CancellationToken cancellationToken);

        Task<Employee> UpdateRole(int employeeId, int newRoleId, CancellationToken cancellationToken);

        Task<Employee> UpdateFurlough(int employeeId, int newFurlough, CancellationToken cancellationToken);

        Task<List<Employee>> SearchUsers(EmployeeSearchDto filter, CancellationToken cancellationToken);
    }
}
