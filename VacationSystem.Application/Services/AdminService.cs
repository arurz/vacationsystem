﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Application.Models;
using VacationSystem.Data.Enums;
using VacationSystem.Data.Models;
using VacationSystem.Persistence;

namespace VacationSystem.Application.Services
{
    public class AdminService : IAdminService
    {
        private readonly VacationSystemDbContext context;

        public AdminService(VacationSystemDbContext context)
        {
            this.context = context;
        }

        public async Task<Request> ChangeRequestStatus(int id, Status status, int userId, CancellationToken cancellationToken)
        {
            var request = await context.Requests.SingleOrDefaultAsync(r => r.Id == id, cancellationToken);

            if (userId == request.EmployeeId)
            {
                throw new Exception("You can`t change status for your own request!");
            }

            else
            {
                request.Status = status;
                if (request.Status == Status.Approved)
                {
                    var employee = await context.Employees.SingleOrDefaultAsync(e => e.Id == request.EmployeeId, cancellationToken);
                    employee.FurloughLeft -= request.NumberOfDays;
                }
            }
            context.Requests.Update(request);
            await context.SaveChangesAsync(cancellationToken);
            return request;
        }

        public async Task<Request> CreateCorrectionForRequest(Correction correction, CancellationToken cancellationToken)
        {
            var request = await context.Requests.SingleOrDefaultAsync(r => r.Id == correction.RequestId, cancellationToken);
            request.Status = Status.AwaitingCorrection;

            context.Requests.Update(request);
            await context.Corrections.AddAsync(correction, cancellationToken);
            await context.SaveChangesAsync(cancellationToken);

            return request;
        }

        public async Task<int> GetMaxFurloughLeft(CancellationToken cancellationToken) => await context.Employees
               .Select(r => r.FurloughLeft)
               .MaxAsync(cancellationToken);

        public async Task<Employee> UpdateRole(int employeeId, int newRoleId, CancellationToken cancellationToken)
        {
            var employee = await context.Employees
                                        .Where(e => e.Id == employeeId)
                                        .Include(e => e.Role)
                                        .FirstOrDefaultAsync(cancellationToken);
            employee.RoleId = newRoleId;
            context.Employees.Update(employee);
            await context.SaveChangesAsync(cancellationToken);
            return employee;
        }

        public async Task<Employee> UpdateFurlough(int employeeId, int newFurlough, CancellationToken cancellationToken)
        {
            var employee = await context.Employees
                                        .Where(e => e.Id == employeeId)
                                        .FirstOrDefaultAsync(cancellationToken);
            employee.FurloughLeft = newFurlough;
            context.Employees.Update(employee);
            await context.SaveChangesAsync(cancellationToken);
            return employee;
        }

        public async Task<List<Employee>> SearchUsers(EmployeeSearchDto filter, CancellationToken cancellationToken)
        {
            var users = context.Employees
                .Include(r => r.Role)
                .AsQueryable();
            var employeeList = new List<Employee>();

            if (!string.IsNullOrEmpty(filter.Firstname))
            {
                users = users.Where(e => e.Firstname.ToLower().Contains(filter.Firstname.ToLower()));
            }

            if (!string.IsNullOrEmpty(filter.Middlename))
            {
                users = users.Where(e => e.Middlename.ToLower().Contains(filter.Middlename.ToLower()));
            }

            if (!string.IsNullOrEmpty(filter.Lastname))
            {
                users = users.Where(e => e.Lastname.ToLower().Contains(filter.Lastname.ToLower()));
            }

            if (!string.IsNullOrEmpty(filter.Username))
            {
                users = users.Where(e => e.Username.ToLower().Contains(filter.Username.ToLower()));
            }

            if (!string.IsNullOrEmpty(filter.Email))
            {
                users = users.Where(e => e.Email.ToLower().Contains(filter.Email.ToLower()));
            }

            if (!string.IsNullOrEmpty(filter.Position))
            {
                users = users.Where(e => e.Position.ToLower().Contains(filter.Position.ToLower()));
            }

            if (!string.IsNullOrEmpty(filter.Role.ToString()) && filter.Role!=-1)
            {
                users = users.Where(ap => ap.Role.Id == filter.Role);
            }

            if (!string.IsNullOrEmpty(filter.MostDaysFurloughLeft.ToString()) && filter.MostDaysFurloughLeft != 0)
            {
                users = users.Where(e => e.FurloughLeft <= filter.MostDaysFurloughLeft);
            }

            if (!string.IsNullOrEmpty(filter.LeastDaysFurloughLeft.ToString()) && filter.LeastDaysFurloughLeft != 0)
            {
                users = users.Where(e => e.FurloughLeft >= filter.LeastDaysFurloughLeft);
            }

            return await users.ToListAsync(cancellationToken); ;
        }
    }
}
