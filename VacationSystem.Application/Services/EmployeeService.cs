﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Application.Models;
using VacationSystem.Persistence;

namespace VacationSystem.Application.Services
{
    class EmployeeService : IEmployeeService
    {
        private readonly VacationSystemDbContext context;
        public EmployeeService(VacationSystemDbContext context)
        {
            this.context = context;
        }


        public async Task<EmployeeDto> GetEmployeeById(int id, CancellationToken cancellationToken)
        {
            var employeeDto = new EmployeeDto();
            var employee = await context.Employees
                                        .Where(e => e.Id == id)
                                        .Include(e=>e.Role)
                                        .SingleOrDefaultAsync(cancellationToken);

            var requests = await context.Requests
                                        .Where(r => r.EmployeeId == id)
                                        .OrderByDescending(r => r.Id)
                                        .ToListAsync(cancellationToken);

            employeeDto.Employee = employee;
            employeeDto.Requests = requests;

            return employeeDto;
        }
    }
}
