﻿using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Data.Models;
using VacationSystem.Persistence;

namespace VacationSystem.Application.Services
{
    public class RoleService : IRoleService
    {
        private readonly VacationSystemDbContext context;
        public RoleService(VacationSystemDbContext context)
        {
            this.context = context;
        }

        public async Task CreateRole(Role role, CancellationToken cancellationToken)
        {
            await context.Roles.AddAsync(role, cancellationToken);
            await context.SaveChangesAsync(cancellationToken);
        }

        public async Task DeleteRole(int id, CancellationToken cancellationToken)
        {
            var role = await context.Roles.SingleOrDefaultAsync(r => r.Id == id, cancellationToken);

            context.Roles.Remove(role);
            await context.SaveChangesAsync(cancellationToken);
        }
    }
}
