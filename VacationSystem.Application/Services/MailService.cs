﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Persistence;

namespace VacationSystem.Application.Services
{
    public class MailService : IMailService
    {
        private readonly VacationSystemDbContext context;
        public MailService(VacationSystemDbContext context)
        {
            this.context = context;
        }

        public async Task SendMail(int employeeId, string correctionDetails, string status, CancellationToken cancellationToken)
        {
            var employee = await context.Employees.SingleOrDefaultAsync(u => u.Id == employeeId, cancellationToken);

            var toAddress = new MailAddress(employee.Email); 
            var fromAddress = new MailAddress("ggamestore1@gmail.com");
            MailMessage message = new(fromAddress, toAddress);

            message.Subject = "Status change";
            message.Body = employee.Firstname + " " +
                           employee.Lastname  + ", your request status was changed on " + status  + ". ";

            if(correctionDetails != null)
            {
                message.Body += " Correction details: " + correctionDetails;
            }

            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;

            SmtpClient client = new("smtp.gmail.com", 587);
            NetworkCredential basicCredemtial = new(fromAddress.Address, "GameStore126578934ProjectMaiL");

            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = basicCredemtial;

            try
            {
                client.SendAsync(message, cancellationToken);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
