﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Application.Models;
using VacationSystem.Data.Models;
using VacationSystem.Persistence;

namespace VacationSystem.Application.Services
{
    public class LoginService : ILoginService
    {
        private readonly VacationSystemDbContext context;
        public LoginService(VacationSystemDbContext context)
        {
            this.context = context;
        }

        public async Task<Employee> LoginAsync(LoginDto model, CancellationToken cancellationToken)
        {
            Employee person = await context.Employees
                .Include(p => p.Role)
                .Include(p => p.Credential)
                .SingleOrDefaultAsync(e => e.Username == model.Username, cancellationToken);

            if(person == null)
            {
                throw new InvalidOperationException("Login Failed");
            }
            var hashedPassword = Task.Run(() => HashService.GenerateHashString(model.Password, person.Credential.Salt));
           
            return person.Credential.Password == await hashedPassword ? person : null;
        }
    }
}
