﻿using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Data.Models;
using VacationSystem.Persistence;

namespace VacationSystem.Application.Services
{
    public class LoggingService : ILoggingService
    {
        private readonly VacationSystemDbContext context;
        public LoggingService(VacationSystemDbContext context)
        {
            this.context = context;
        }

        public async Task LogException(Exception exception, HttpContext httpContext)
        {
            await Log(exception.Message, httpContext);
        }

        public async Task LogRequest(HttpContext httpContext)
        {
            await Log(null, httpContext);
        }

        public async Task Log(string message, HttpContext httpContext)
        {

            var userId = httpContext.Request.Headers
                .Where(x => x.Key.ToLower() == "userid")
                .FirstOrDefault().Value
                .SingleOrDefault();

            var log = new Log
            {
                StatusCode = httpContext.Response.StatusCode,
                AppearenceDateTime = DateTime.UtcNow,
                Verb = httpContext.Request.Method,
                Url = httpContext.Request.Path,
                Message = message
            };

            if(userId != null)
            {
                log.UserId = int.Parse(userId);
            }

            await context.Logs.AddAsync(log);
            await context.SaveChangesAsync();
        }
    }
}
