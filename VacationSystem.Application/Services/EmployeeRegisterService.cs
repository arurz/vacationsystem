﻿using System;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Data.Models;
using VacationSystem.Persistence;

namespace VacationSystem.Application.Services
{
    public class EmployeeRegisterService : IEmployeeRegistrationService
    {
        const int  totalDaysFurlough = 20;
        const int totalMonths = 12;
        private readonly VacationSystemDbContext context;
        public EmployeeRegisterService(VacationSystemDbContext context)
        {
            this.context = context;
        }

        public async Task RegisterEmployee(Employee employee, CancellationToken cancellationToken)
        {
            if(employee.Credential.Password != employee.Credential.RepeatPassword)
            {
                throw new Exception("Passwords mismatch");
            }

            byte[] Salt = {};
            var hashedPassword = Task.Run(() => HashService.GenerateHashString(employee.Credential.Password, ref Salt));

            employee.RoleId = 1;
            employee.Credential.Password = await hashedPassword;
            employee.Credential.Salt = Salt;

            int monthsLeft = totalMonths - int.Parse(DateTime.Now.ToString("MM"))+1;

            employee.FurloughLeft = Convert.ToInt32(Math.Ceiling((double)monthsLeft * totalDaysFurlough / totalMonths));

            employee.Credential = employee.Credential;
            await context.AddAsync(employee, cancellationToken);
            
            await context.SaveChangesAsync(cancellationToken);
        }
    }
}
