﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Data.Models;

namespace VacationSystem.Application.Services
{
    public class JwtService : IJwtService
    {
        private readonly IConfiguration config;
        public JwtService(IConfiguration config)
        {
            this.config = config;
        }

        public async Task<string> GenerateJwt(Employee person, CancellationToken cancellationToken)
        {
            var securityKey = await Task.Run(() => new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["Jwt:Key"])),cancellationToken);
            var credentials = await Task.Run(() => new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256),cancellationToken);

            var claims = new[]
            {
                new Claim("userId", person.Id.ToString()),
                new Claim("name", person.Username),
                new Claim("role", person.Role.Name)
            };

            var token = new JwtSecurityToken(config["Jwt:Issuer"],
                config["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddHours(24.0),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
