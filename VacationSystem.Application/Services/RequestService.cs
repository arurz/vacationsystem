﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Data.Models;
using VacationSystem.Persistence;

namespace VacationSystem.Application.Services
{
    public class RequestService : IRequestService
    {
        private readonly VacationSystemDbContext context;
        public RequestService(VacationSystemDbContext context)
        {
            this.context = context;
        }

        public async Task<List<Request>> GetAllRequests(CancellationToken cancellationToken) => await context.Requests
            .OrderByDescending(r => r.Id)
            .ToListAsync(cancellationToken);

        public async Task<Request> GetRequestById(int id, CancellationToken cancellationToken) 
        {
            return await context.Requests.Include(r => r.Employee)
                                         .Include(r => r.Corrections)
                                            .ThenInclude(c=>c.Employee)
                                         .SingleOrDefaultAsync(r => r.Id == id, cancellationToken);
        }

        public async Task<int> GetMaxVacationDays(CancellationToken cancellationToken) => await context.Requests
               .Select(r => r.NumberOfDays)
               .MaxAsync(cancellationToken);

        public static void CheckDates(Request request)
        {
            if (!CheckValidYear(request.Year))
            {
                throw new ArgumentException("Year is not valid");
            }

            if (!CheckValidYear(request.StartDate.Year))
            {
                throw new ArgumentException("Start date is not valid");
            }

            if (!CheckValidYear(request.FinishDate.Year))
            {
                throw new ArgumentException("Finish date is not valid");
            }

            if (!CheckValidYear(request.RequestDate.Year))
            {
                throw new ArgumentException("Request date is not valid");
            }
        }

        public async Task<Request> CreateRequest(Request request, CancellationToken cancellationToken)
        {
            CheckDates(request);

            var employee = await context.Employees.SingleOrDefaultAsync(e => e.Id == request.EmployeeId, cancellationToken);
            request.NumberOfDays = request.FinishDate.Subtract(request.StartDate).Days + 1;

            if (request.NumberOfDays < 1)
            {
                throw new Exception("Number of vacation days can`t be negative!");
            }

            if (employee.FurloughLeft - request.NumberOfDays < 0)
            {
                throw new Exception("You don't have enough days (" + request.NumberOfDays + ") for this vacation");
            }
            request.Employee = employee;
            await context.Requests.AddAsync(request, cancellationToken);
            await context.SaveChangesAsync(cancellationToken);

            return request;
        }

        public async Task DeleteRequest(int id, CancellationToken cancellationToken)
        {
            var request = await context.Requests.SingleOrDefaultAsync(r => r.Id == id, cancellationToken);

            context.Requests.Remove(request);
            await context.SaveChangesAsync(cancellationToken);
        }

        public async Task<Request> UpdateRequest(Request request, CancellationToken cancellationToken)
        {
            if(request.Status == Data.Enums.Status.AwaitingCorrection)
            {
                CheckDates(request);

                request.NumberOfDays = request.FinishDate.Subtract(request.StartDate).Days + 1;

                if (request.NumberOfDays < 1)
                {
                    throw new Exception("Number of vacation days can`t be negative!");
                }

                var employee = await context.Employees
                    .AsNoTracking()
                    .SingleOrDefaultAsync(e => e.Id == request.EmployeeId, cancellationToken);

                if (employee.FurloughLeft - request.NumberOfDays < 0)
                {
                    throw new Exception("You don't have enough days (" + request.NumberOfDays + ") for this vacation");
                }

                request.Status = Data.Enums.Status.Pending;

                context.Requests.Update(request);
                await context.SaveChangesAsync(cancellationToken);
            }

            return request;
        }

        private static bool CheckValidYear(int year)
        {
            var currentYear = int.Parse(DateTime.Now.Year.ToString());
            if (year > currentYear + 1 || year < currentYear - 1)
            {
                return false;
            }
            return true;
        }
    }
}
