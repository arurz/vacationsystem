﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Application.Models;
using VacationSystem.Persistence;

namespace VacationSystem.Application.Services
{
    public class SearchService : ISearchService
    {
        private readonly VacationSystemDbContext context;
        public SearchService(VacationSystemDbContext context)
        {
            this.context = context;
        }

        public async Task<List<RequestDto>> SearchRequests(SearchDto searchDto, CancellationToken cancellationToken)
        {
            var requests = context.Requests
                .Include(r => r.Employee)
                .OrderByDescending(r => r.Id)
                .AsQueryable();


            if (!string.IsNullOrEmpty(searchDto.Year.ToString()) && searchDto.Year != 0)
            {
                requests = requests.Where(r => r.Year == searchDto.Year);
            }

            if (!string.IsNullOrEmpty(searchDto.EmployeeUsername))
            {
                requests = requests.Where(r => r.Employee.Username.ToLower().Contains(searchDto.EmployeeUsername.ToLower()));
            }

            if (!string.IsNullOrEmpty(searchDto.MostNumberOfDays.ToString()) && searchDto.MostNumberOfDays != 0)
            {
                requests = requests.Where(r => r.NumberOfDays <= searchDto.MostNumberOfDays);
            }

            if (!string.IsNullOrEmpty(searchDto.LeastNumberOfDays.ToString()) && searchDto.LeastNumberOfDays != 0)
            {
                requests = requests.Where(r => r.NumberOfDays >= searchDto.LeastNumberOfDays);
            }

            if (!string.IsNullOrEmpty(searchDto.Status.ToString()) && searchDto.Status != -1)
            {
                requests = requests.Where(ap => ((int)ap.Status) == searchDto.Status);
            }

            if (!string.IsNullOrEmpty(searchDto.RequestTopDate.ToString()) && searchDto.RequestTopDate.Year != 1)
            {
                requests = requests.Where(r => r.RequestDate <= searchDto.RequestTopDate);
            }

            if (!string.IsNullOrEmpty(searchDto.RequestBottomDate.ToString()) && searchDto.RequestBottomDate.Year != 1)
            {
                requests = requests.Where(r => r.RequestDate >= searchDto.RequestBottomDate);
            }

            var requestDtoList = requests.Select(r =>
                new RequestDto
                {
                    RequestId = r.Id,
                    Year = r.Year,
                    RequestCreationDate = r.RequestDate,
                    VacationStartDate = r.StartDate,
                    VacationFinalDate = r.FinishDate,
                    NumberOfVacationDays = r.NumberOfDays,
                    EmployeeUsername = r.Employee.Username,
                    StatusString = r.Status.ToString()
                }).ToList();

            return requestDtoList;
        }
    }
}
