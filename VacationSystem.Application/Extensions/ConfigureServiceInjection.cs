﻿using Microsoft.Extensions.DependencyInjection;
using VacationSystem.Application.Interfaces;
using VacationSystem.Application.Services;

namespace VacationSystem.Application.Extensions
{
    public static class ConfigureServiceInjection
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<HashService>();
            services.AddScoped<IJwtService, JwtService>();
            services.AddScoped<ILoggingService, LoggingService>();
            services.AddScoped<IMailService, MailService>();

            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IEmployeeRegistrationService, EmployeeRegisterService>();

            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<ISearchService, SearchService>();
            services.AddScoped<IRequestService, RequestService>();

            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<IAdminService, AdminService>();

            return services;
        }
    }
}
