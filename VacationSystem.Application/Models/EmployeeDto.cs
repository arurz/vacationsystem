﻿using System.Collections.Generic;
using VacationSystem.Data.Models;

namespace VacationSystem.Application.Models
{
    public class EmployeeDto
    {
        public Employee Employee { get; set; }
        public List<Request> Requests { get; set; }
    }
}
