﻿namespace VacationSystem.Application.Models
{
    public class EmployeeSearchDto
    {
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public int Role { get; set; }
        public string Position { get; set; }
        public int MostDaysFurloughLeft { get; set; }
        public int LeastDaysFurloughLeft { get; set; }
    }
}
