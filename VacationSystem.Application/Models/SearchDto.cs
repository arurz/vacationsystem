﻿using System;

namespace VacationSystem.Application.Models
{
    public class SearchDto
    {
        public int Year { get; set; }

        public DateTime RequestTopDate { get; set; }
        public DateTime RequestBottomDate { get; set; }

        public int MostNumberOfDays { get; set; }
        public int LeastNumberOfDays { get; set; }

        public string EmployeeUsername { get; set; }
        public int Status { get; set; }
    }
}
