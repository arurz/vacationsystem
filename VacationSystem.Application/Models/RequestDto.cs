﻿using System;

namespace VacationSystem.Application.Models
{
    public class RequestDto
    {
        public int RequestId { get; set; }
        public int Year { get; set; }

        public DateTime RequestCreationDate { get; set; }
        public DateTime VacationStartDate { get; set; }
        public DateTime VacationFinalDate { get; set; }

        public int NumberOfVacationDays { get; set; }
        public string EmployeeUsername { get; set; }
        public string StatusString { get; set; }
    }
}
