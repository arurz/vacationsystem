﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VacationSystem.Persistence.Migrations
{
    public partial class Application_Changed_To_Request1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Corrections_Requests_RequestId",
                table: "Corrections");

            migrationBuilder.DropColumn(
                name: "ApplicationId",
                table: "Corrections");

            migrationBuilder.AlterColumn<int>(
                name: "RequestId",
                table: "Corrections",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Corrections_Requests_RequestId",
                table: "Corrections",
                column: "RequestId",
                principalTable: "Requests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Corrections_Requests_RequestId",
                table: "Corrections");

            migrationBuilder.AlterColumn<int>(
                name: "RequestId",
                table: "Corrections",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<int>(
                name: "ApplicationId",
                table: "Corrections",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_Corrections_Requests_RequestId",
                table: "Corrections",
                column: "RequestId",
                principalTable: "Requests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
