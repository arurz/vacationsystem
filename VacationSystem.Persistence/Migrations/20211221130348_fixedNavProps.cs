﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VacationSystem.Persistence.Migrations
{
    public partial class fixedNavProps : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Credentials_EmployeeId",
                table: "Credentials",
                column: "EmployeeId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Credentials_Employees_EmployeeId",
                table: "Credentials",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Credentials_Employees_EmployeeId",
                table: "Credentials");

            migrationBuilder.DropIndex(
                name: "IX_Credentials_EmployeeId",
                table: "Credentials");
        }
    }
}
