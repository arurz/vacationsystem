﻿using Microsoft.EntityFrameworkCore;
using VacationSystem.Data.Models;

namespace VacationSystem.Persistence
{
    public class VacationSystemDbContext : DbContext
    {

        public DbSet<Request> Requests{ get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Correction> Corrections { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Credential> Credentials { get; set; }
        public DbSet<Log> Logs { get; set; }

        public VacationSystemDbContext(DbContextOptions options) : base(options)
        {   }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(CorrectionConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(EmployeeConfiguration).Assembly);
        }
    }
}
