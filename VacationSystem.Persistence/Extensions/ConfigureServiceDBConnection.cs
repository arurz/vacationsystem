﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VacationSystem.Persistence;

namespace VacationSystem.Hosting.Extensions
{
    public static class ConfigureServiceDBConnection
    {
        public static IServiceCollection AddDBConnection(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<VacationSystemDbContext>(
                options => options.UseNpgsql(configuration["DBConnection:ConnectionString"]));

            return services;
        }
    }
}
