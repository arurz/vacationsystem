﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Application.Models;
using VacationSystem.Data.Enums;
using VacationSystem.Data.Models;

namespace VacationSystem.Hosting.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AdminController : ControllerBase
    {
        private readonly IAdminService adminService;
        private readonly IMailService mailService;

        public AdminController(IAdminService adminService, IMailService mailService)
        {
            this.adminService = adminService;
            this.mailService = mailService;
        }

        [HttpPut("request/change/status/{id}")]
        public async Task<Request> ChangeRequestStatus([FromRoute] int id, [FromBody] Status status, CancellationToken cancellationToken)
        {
            var userId = Request.Headers
                .Where(x => x.Key.ToLower() == "userid")
                .FirstOrDefault().Value
                .SingleOrDefault();

            if (userId != null)
            {
                var request = await adminService.ChangeRequestStatus(id, status, int.Parse(userId),cancellationToken);
                if (request != null)
                {
                    await mailService.SendMail(request.EmployeeId, null, Enum.GetName(status), cancellationToken);
                }
                return request;
            }

            throw new Exception("User id wasn`t submitted to request headers");
        }

        [HttpPut("request/needCorrections")]
        public async Task<Request> ReturnRequestForCorrections([FromBody] Correction correction, CancellationToken cancellationToken)
        {
            var request = await adminService.CreateCorrectionForRequest(correction,cancellationToken);
            if (request != null)
            {
                await mailService.SendMail(request.EmployeeId, correction.Reason, "Awaiting for Correction", cancellationToken);
            }
            return request;
        }

        [HttpGet("users")]
        public async Task<List<Employee>> SearchUsers([FromQuery] EmployeeSearchDto dto, CancellationToken cancellationToken) => await adminService.SearchUsers(dto,cancellationToken);

        [HttpGet("users/maxFurlough")]
        public async Task<int> GetMaxFurloughLeft(CancellationToken cancellationToken) => await adminService.GetMaxFurloughLeft(cancellationToken);

        [HttpPut("change/role/{id}")]
        public async Task<Employee> UpdateRole(int id, [FromBody] int newRoleId, CancellationToken cancellationToken) => await adminService.UpdateRole(id, newRoleId,cancellationToken);

        [HttpPut("change/furlough/{id}")]
        public async Task<Employee> UpdateFurlough(int id, [FromBody] int newFurlough, CancellationToken cancellationToken) => await adminService.UpdateFurlough(id, newFurlough,cancellationToken);
    }
}
