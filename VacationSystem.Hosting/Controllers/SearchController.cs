﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Application.Models;

namespace VacationSystem.Hosting.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SearchController : ControllerBase
    {
        private readonly ISearchService searchService;
        public SearchController(ISearchService searchService)
        {
            this.searchService = searchService;
        }

        [HttpGet("requests")]
        public async Task<List<RequestDto>> SearchRequests([FromQuery] SearchDto dto, CancellationToken cancellationToken) => await searchService.SearchRequests(dto, cancellationToken);
    }
}
