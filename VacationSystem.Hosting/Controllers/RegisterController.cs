﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Data.Models;

namespace VacationSystem.Hosting.Controllers
{
    [AllowAnonymous]
    [ApiController]
    [Route("api/[controller]")]
    public class RegisterController : ControllerBase
    {
        private readonly IEmployeeRegistrationService employeeRegistrationService;

        public RegisterController(IEmployeeRegistrationService employeeRegistrationService)
        {
            this.employeeRegistrationService = employeeRegistrationService;
        }

        [HttpPost("employee")]
        public async Task<Employee> RegisterEmployee([FromBody] Employee employee, CancellationToken cancellationToken)
        {
            await employeeRegistrationService.RegisterEmployee(employee, cancellationToken);

            return employee;
        }
    }
}
