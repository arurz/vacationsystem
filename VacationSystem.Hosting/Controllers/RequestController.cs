﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Data.Models;

namespace VacationSystem.Hosting.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RequestController : ControllerBase
    {
        private readonly IRequestService requestService;
        public RequestController(IRequestService requestService)
        {
            this.requestService = requestService;
        }

        [HttpGet("{id}")]
        public async Task<Request> GetRequestById([FromRoute] int id, CancellationToken cancellationToken) => await requestService.GetRequestById(id, cancellationToken);

        [HttpGet("all")]
        public async Task<List<Request>> GetAllRequests(CancellationToken cancellationToken) => await requestService.GetAllRequests(cancellationToken);

        [HttpGet("vacationDays/max")]
        public async Task<int> GetMaxVacationDays(CancellationToken cancellationToken) => await requestService.GetMaxVacationDays(cancellationToken);

        [HttpPost("create")]
        public async Task<Request> CreateRequest([FromBody] Request request, CancellationToken cancellationToken) => await requestService.CreateRequest(request, cancellationToken);

        [HttpDelete("delete/{id}")]
        public async Task DeleteRequest([FromRoute] int id, CancellationToken cancellationToken) => await requestService.DeleteRequest(id, cancellationToken);

        [HttpPut("edit")]
        public async Task<Request> EditRequestAfterCorrection([FromBody] Request request, CancellationToken cancellationToken) => await requestService.UpdateRequest(request, cancellationToken);

    }
}
