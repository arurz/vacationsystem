﻿using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Data.Models;

namespace VacationSystem.Hosting.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService roleService;
        public RoleController(IRoleService roleService)
        {
            this.roleService = roleService;
        }

        [HttpPost("create")]
        public async Task CreateRole([FromBody] Role role, CancellationToken cancellationToken) => await roleService.CreateRole(role,cancellationToken);

        [HttpDelete("delete/{id}")]
        public async Task DeleteRole([FromRoute] int id, CancellationToken cancellationToken) => await roleService.DeleteRole(id,cancellationToken);

    }
}
