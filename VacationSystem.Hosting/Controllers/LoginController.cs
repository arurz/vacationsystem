﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;
using VacationSystem.Application.Models;
using VacationSystem.Data.Models;

namespace VacationSystem.Hosting.Controllers
{
    [AllowAnonymous]
    [ApiController]
    [Route("api/[controller]")]
    public class LoginController : ControllerBase
    {
        private readonly ILoginService loginService;
        private readonly IJwtService jwtService;
        public LoginController(ILoginService loginService, IJwtService jwtService)
        {
            this.loginService = loginService;
            this.jwtService = jwtService;
        }

        [HttpPost]
        public async Task<ActionResult<Employee>> Login([FromBody] LoginDto model, CancellationToken cancellationToken)
        {
            var person = await loginService.LoginAsync(model, cancellationToken);
            if(person != null)
            {
                var tokenString = await jwtService.GenerateJwt(person, cancellationToken);
                return Ok( new { token = tokenString });
            }

            throw new Exception("Login Failed");
        }
    }
}
