﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Net;
using System.Threading.Tasks;
using VacationSystem.Application.Interfaces;

namespace VacationSystem.Hosting.Middlewares
{
    public class RequestLoggingMiddleware
    {
		private readonly RequestDelegate next;

		public RequestLoggingMiddleware(RequestDelegate next)
		{
			this.next = next;
		}

		public async Task Invoke(HttpContext httpContext, ILoggingService logger)
		{
			try
			{
				await next(httpContext);
				await logger.LogRequest(httpContext);
			}
			catch (Exception ex)
			{
				await logger.LogException(ex, httpContext);
				await HandleExceptionAsync(httpContext, HttpStatusCode.InternalServerError, ex);
			}
		}
		private static Task HandleExceptionAsync(HttpContext context, HttpStatusCode statusCode, object content)
		{
			context.Response.Clear();
			context.Response.StatusCode = (int)statusCode;
			context.Response.ContentType = "application/json";
			context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
			var responseText = JsonConvert.SerializeObject(content, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

			return context.Response.WriteAsync(responseText);
		}
	}
}
