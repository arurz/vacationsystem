import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Correction } from '../models/Correction';
import { Status } from '../models/enums/Status';
import { Request } from '../models/request';
import { RequestModel } from '../models/request.model';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  readonly url = '/api/request';
  readonly adminUrl = '/api/admin';
  constructor(private http: HttpClient) { }

  createRequest(requestForm: RequestModel): Observable<any> {
    requestForm.numberOfDays = 0;
    requestForm.year = Number(requestForm.year);
    return this.http.post<any>(this.url + '/create', requestForm);
  }

  getMaxVacationDays(): Observable<number> {
    return this.http.get<number>(this.url + "/vacationDays/max");
  }

  getRequest(id: number): Observable<RequestModel> {
    return this.http.get<RequestModel>(this.url + '/' + id.toString());
  }

  ChangeStatusForCorrection(correctionForm: Correction): Observable<Correction> {
    return this.http.put<Correction>(this.adminUrl + "/request/needCorrections", correctionForm);
  }

  ChangeStatusOnApproveOrDecline(id: number, status: Status): Observable<Request> {
    return this.http.put<Request>(this.adminUrl + "/request/change/status/" + id, status);
  }

  updateRequestAfterCorrection(requestModel: Request): Observable<Request> {
    return this.http.put<Request>(this.url + '/edit', requestModel);
  }
}

