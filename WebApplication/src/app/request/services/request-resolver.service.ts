import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class RequestResolverService implements Resolve<any> {

  constructor(private requestService: RequestService) { }
  resolve(route: ActivatedRouteSnapshot) {
    return this.requestService.getRequest(+route.params['id']);
  }
}
