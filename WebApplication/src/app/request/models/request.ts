export class Request {
    id: number;
    requestDate: string;
    year: number;
    numberOfDays: number;
    startDate: string;
    finishDate: string;
    status: number;
    employeeId: number;
}
