export class Correction {
  id: number;
  correctionDate: Date;
  reason: string;
  employeeId: number;
  requestId: number;
}