import { Employee } from 'src/app/user/models/Employee';
import { Correction } from './Correction';

export class RequestModel {
    id: number;
    requestDate: string;
    year: number;
    numberOfDays: number;
    startDate: string;
    finishDate: string;
    status: number;

    employeeId: number;
    employee: Employee = new Employee();
    
    statusString: string;
    corrections: Correction[];
}
