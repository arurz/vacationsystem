export enum Status {
  Pending,
  Approved,
  AwaitingCorrection,
  Declined
}