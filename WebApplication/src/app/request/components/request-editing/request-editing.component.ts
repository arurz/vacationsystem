import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Request } from '../../models/request';
import { RequestModel } from '../../models/request.model';
import { RequestService } from '../../services/request.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from 'src/app/common/services/notification.service';


@Component({
  selector: 'app-request-editing',
  templateUrl: './request-editing.component.html',
  styleUrls: ['./request-editing.component.css']
})
export class RequestEditingComponent implements OnInit {
  private sub: any;
  id: number;

  requestModel: RequestModel = new RequestModel();
  constructor(private requestService: RequestService,
    private route: ActivatedRoute,
    private spinnerService: NgxSpinnerService,
    private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.spinnerService.show();
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
    });
    this.getRequest();
  }

  getRequest() {
    this.requestService.getRequest(this.id)
      .subscribe(requestModel => {
        this.requestModel = requestModel;
        this.spinnerService.hide();
      });
  }

  editRequest() {
    this.spinnerService.show();

    let request: Request = new Request();
    request.id = this.requestModel.id;
    request.requestDate = this.requestModel.requestDate;
    request.year = this.requestModel.year;
    request.numberOfDays = this.requestModel.numberOfDays;
    request.startDate = this.requestModel.startDate;
    request.finishDate = this.requestModel.finishDate;
    request.status = this.requestModel.status;
    request.employeeId = this.requestModel.employeeId;

    this.requestService.updateRequestAfterCorrection(request)
      .subscribe(() => {
        this.spinnerService.hide();
        this.notificationService.showSuccess("Request changed", "Vacation request correction");
      });
      this.spinnerService.hide();
  }

}
