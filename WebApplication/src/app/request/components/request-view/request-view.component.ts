import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RequestModel } from '../../models/request.model';
import { RequestService } from '../../services/request.service';
import { Status } from '../../models/enums/Status';
import { Globals } from 'src/app/common/globals';
import { Correction } from '../../models/Correction';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from 'src/app/common/services/notification.service';

@Component({
  selector: 'app-request-view',
  templateUrl: './request-view.component.html',
  styleUrls: ['./request-view.component.css']
})
export class RequestViewComponent implements OnInit {
  private sub: any;
  id: number;
  adminRequest: boolean;
  ownRequest: boolean;
  awaiting: boolean;

  request: RequestModel = new RequestModel();
  Correction: Correction = new Correction();

  AdminCorrection: boolean = false;
  EmployeeCorrection: boolean = false;
  requestClosed: boolean = false;

  constructor(private route: ActivatedRoute,
    private requestService: RequestService,
    public globals: Globals,
    private spinnerService: NgxSpinnerService,
    private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.spinnerService.show();
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
    });
    this.getRequest();
  }

  getRequest(): void {
    this.route.data.subscribe((response: any) => {
      this.request = response.products;
      this.request.statusString = Status[this.request.status];
        if (this.request.status == 2 && this.globals.isUser) {
            this.EmployeeCorrection = true;
        }
        this.requestClosed = (this.request.status === 1 || this.request.status === 3) ? true : false;
        this.spinnerService.hide();
        this.checkIfAdminRequest();
    })
  }

  AddCorrectionReqson() {
    this.AdminCorrection = true;
  }

  ChangeStatusForCorrection() {
    this.spinnerService.show();

    let today = new Date();
    this.Correction.correctionDate = today;

    let token: any = localStorage.getItem('token');
    let decodedJwt = JSON.parse(window.atob(token.split('.')[1]))
    this.Correction.employeeId = decodedJwt.userId;

    this.Correction.requestId = this.id;
    this.requestService.ChangeStatusForCorrection(this.Correction)
      .subscribe((res: Correction) => {
        this.request.corrections.push(res);
        this.request.statusString = Status[2];
        this.Correction = new Correction();
        this.spinnerService.hide();
        this.notificationService.showInfo("Correction reason added", "Vacation request correction");
        
        location.reload();
      })
  }

  ApproveRequest() {
    this.spinnerService.show();

    this.requestService.ChangeStatusOnApproveOrDecline(this.id, Status.Approved)
      .subscribe(() => {
        this.request.statusString = Status[1];
        this.spinnerService.hide();
        this.notificationService.showSuccess("Request approved", "Vacation status changed");
        
        location.reload();
      });
  }

  DeclineRequest() {
    this.spinnerService.show();
    
    this.requestService.ChangeStatusOnApproveOrDecline(this.id, Status.Declined)
      .subscribe(() => {
        this.request.statusString = Status[3];
        this.notificationService.showSuccess("Request declined", "Vacation status changed");
        this.spinnerService.hide();
        
        location.reload();
      });
  }

  checkIfAdminRequest() {
    const token: any = localStorage.getItem("token");
    let decodedJWT = JSON.parse(window.atob(token.split('.')[1]));
    let id = Number(decodedJWT.userId);
    let role = decodedJWT.role.toString();

    if (this.request.employeeId === id) {
        this.ownRequest = true;
        if (role === "admin") {
            this.adminRequest = true;
        }
        if (this.request.status === Status.AwaitingCorrection) {
            this.awaiting = true;
        }
    }
  }
}
