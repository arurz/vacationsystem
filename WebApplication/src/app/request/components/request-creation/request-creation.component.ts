import { Component, OnInit } from '@angular/core';
import { RequestModel } from '../../models/request.model';
import { RequestService } from '../../services/request.service';
import { NotificationService } from 'src/app/common/services/notification.service';

@Component({
  selector: 'app-request-creation',
  templateUrl: './request-creation.component.html',
  styleUrls: ['./request-creation.component.css']
})
export class RequestCreationComponent implements OnInit {

  requestForm: RequestModel = new RequestModel();
  constructor(private requestService: RequestService,
    private notificationService: NotificationService) { }

  ngOnInit(): void {
  }

  createRequest() {
    let token: any = localStorage.getItem('token');
    let decodedJwt = JSON.parse(window.atob(token.split('.')[1]))

    const userid: number = decodedJwt.userId;
    this.requestForm.employeeId = userid;

    this.requestService.createRequest(this.requestForm)
      .subscribe((res: any) => {
        console.log(res);
        this.notificationService.showSuccess("Request created", "Vacation Request Creation");
      });
    this.requestForm = new RequestModel();
  }
}
