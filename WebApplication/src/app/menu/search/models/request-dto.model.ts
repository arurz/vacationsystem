export class RequestDto {
    requestId: number;
    year: number;
    numberOfVacationDays: number;

    requestCreationDate: Date;
    vacationStartDate: Date;
    vacationFinalDate: Date;
    
    employeeUsername: string;
    statusString: string;
}
