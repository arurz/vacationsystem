export class SearchDto {
    year: number;
    requestTopDate: Date;
    requestBottomDate: Date
    mostNumberOfDays: number;
    leastNumberOfDays: number;
    employeeUsername: string;
    status: number;
}