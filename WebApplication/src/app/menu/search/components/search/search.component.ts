import { Component, OnInit } from '@angular/core';
import { RequestDto } from '../../models/request-dto.model';
import { SearchDto } from '../../models/search-dto.model';
import { SearchService } from '../../services/search.service';
import { Options } from '@angular-slider/ngx-slider';
import { NgxSpinnerService } from 'ngx-spinner';
import { Globals } from 'src/app/common/globals';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  maxVacationDays: number;
  searchDto: SearchDto = new SearchDto();
  requests: RequestDto[] = [];

  p: number = 1;
  count: number = 7;

  minValue: number = 0;
  maxValue: number = 0;

  options: Options = { ceil: 10000 };

  constructor(private searchService: SearchService,
    private spinnerService: NgxSpinnerService,
    private route: ActivatedRoute,
    private globals: Globals) { }

  ngOnInit(): void {
    this.globals.isFirstLoad = true;
    this.searchDto.status = -1;
    this.search();
  }

  setOptions(): void {
    let opts: Options = {
      floor: 0,
      step: 1,
      ceil: this.maxVacationDays,
      showTicks: true
    };
    this.options = opts;
  }

  getMaxVacationDays(): void {
    this.maxVacationDays = this.requests[0].numberOfVacationDays;
    for(let i = 0;i < this.requests.length; i++){
        if(this.requests[i].numberOfVacationDays > this.maxVacationDays){
            this.maxVacationDays = this.requests[i].numberOfVacationDays;
        }
    }
    this.setOptions();
  }

  search() { 
    this.spinnerService.show();

    if(this.globals.isFirstLoad == true){
        this.route.data.subscribe((response: any) => {
        this.requests = response.requests;
        this.getMaxVacationDays();
        this.globals.isFirstLoad = false;
        this.spinnerService.hide();
      });
    }
    else{
      this.searchDto.mostNumberOfDays = this.maxValue;
      this.searchDto.leastNumberOfDays = this.minValue;
    
      this.searchService.searchRequest(this.searchDto)
        .subscribe((requests: RequestDto[]) => {
          this.requests = requests;
          this.spinnerService.hide();
        });
      }
  }

  deleteFilter() {
    this.maxValue = 0;
    this.minValue = 0;
    this.searchDto = new SearchDto();
    this.searchDto.status = -1;

    this.search();
  }
}
