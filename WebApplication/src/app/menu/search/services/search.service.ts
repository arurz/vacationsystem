import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RequestDto } from '../models/request-dto.model';
import { SearchDto } from '../models/search-dto.model';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  readonly url = "/api/search";
  constructor(private http: HttpClient) { }
  composeQueryString(object: any): string {
    let result = '';
    let isFirst = true;
    if (object) {
      Object.keys(object)
        .filter(key => object[key] !== null && object[key] !== undefined)
        .forEach(key => {
          let value = object[key];
          if (value instanceof Date) {
            value = value.toISOString();
          }
          if (isFirst) {
            result = '?' + key + '=' + value;
            isFirst = false;
          } else {
            result += '&' + key + '=' + value;
          }
        });
    }
    return result;
  }

  searchRequest(dto: SearchDto): Observable<RequestDto[]> {
    let query = this.url + "/requests" + this.composeQueryString(dto);
    return this.http.get<RequestDto[]>(query);
  }

}
