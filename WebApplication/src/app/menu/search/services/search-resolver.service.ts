import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { SearchDto } from '../models/search-dto.model';
import { SearchService } from './search.service';

@Injectable({
  providedIn: 'root'
})
export class SearchResolverService implements Resolve<any> {

  filter: SearchDto = new SearchDto();

  constructor(private searchService: SearchService) { }
  resolve(route: ActivatedRouteSnapshot) {

    this.filter.status = -1;
    this.filter.mostNumberOfDays = 0;
    this.filter.leastNumberOfDays = 0;

    return this.searchService.searchRequest(this.filter);
  }
}
