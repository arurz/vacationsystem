import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Globals } from 'src/app/common/globals';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private router: Router, public globals: Globals) { }

  ngOnInit(): void {
    const token: any = localStorage.getItem("token");
    let decodedJWT = JSON.parse(window.atob(token.split('.')[1]));
    if (token) {
        this.globals.isLogged = true;
        if (decodedJWT.role == "admin") {
            this.globals.isSuperAdmin = false;
            this.globals.isAdmin = true;
            this.globals.isUser = false;
        }
        else if (decodedJWT.role == "super-admin") {
            this.globals.isSuperAdmin = true;
            this.globals.isAdmin = true;
            this.globals.isUser = false;
        }
        else {
            this.globals.isSuperAdmin = false;
            this.globals.isAdmin = false;
            this.globals.isUser = true;
        }
    }
  }

  logOut(): void {
    localStorage.removeItem('token');
    this.globals.isAdmin = false;
    this.globals.isLogged = false;

    this.router.navigateByUrl('login');
  }

  viewProfile(): void {
    let token: any = localStorage.getItem('token');
    let decodedJWT = JSON.parse(window.atob(token.split('.')[1]));

    this.router.navigateByUrl('profile/' + decodedJWT.userId);
  }
}
