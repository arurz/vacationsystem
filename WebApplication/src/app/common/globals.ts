import { Injectable } from '@angular/core';

Injectable()
export class Globals {
  isLogged = false;
  isAdmin = false;
  isSuperAdmin = false;
  isUser = false;
  isFirstLoad = true;
}