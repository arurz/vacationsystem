import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, of, tap } from 'rxjs';
import { NotificationService } from '../common/services/notification.service';

export const InterceptorSkipHeader = 'X-Skip-Interceptor';

@Injectable({
  providedIn: 'root'
})

export class HttpConfigInterceptorService implements HttpInterceptor {

  constructor(private notificationService: NotificationService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (request.headers.has(InterceptorSkipHeader)) {
      const headers = request.headers.delete(InterceptorSkipHeader);
      return next.handle(request.clone({ headers }));
    }

    const token: any = localStorage.getItem("token");
    let decodedJWT = JSON.parse(window.atob(token.split('.')[1]));

    const params = request.params;
    let headers = request.headers;

    if (token) {
      headers = headers.set('accessToken', token);
      headers = headers.set('userId', decodedJWT.userId);
    }

    request = request.clone({
      params,
      headers
    });

    return next.handle(request).pipe(
      tap(evt => {
        if (evt instanceof HttpResponse) {
          if (evt.body && evt.body.success)
            this.notificationService.showSuccess(evt.body.statusText, "Vacation System");
        }
      }),
      catchError((err: any) => {

        console.log(err);
        if (err instanceof HttpErrorResponse || err instanceof ErrorEvent) {
          try {
            if (err.error.title == null) {
              this.notificationService.showError(err.error.Message, err.error.ClassName);
            }
            else {
              this.notificationService.showError(err.error.Message, err.error.title);
            }
          } catch (e) {
            this.notificationService.showError('An error occurred', 'Error');
          }
        }
        else {
          if (err.title != undefined) {
            this.notificationService.showError("Error Status: " + err.status + ". Message: " + err.data, 'Error');
          }
          this.notificationService.showError("Error Status: " + err.status, 'Error');
        }
        return of(err);
      }));
  }
}
