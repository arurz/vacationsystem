import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Globals } from '../common/globals';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AdminAuthGuard implements CanActivate {
  constructor(private authService: AuthenticationService,
    private router: Router,
    public globals: Globals) {
  }

  canActivate(): boolean {
    if (this.authService.loggedIn()) {
      let token: any = localStorage.getItem('token');
      let decodedJWT = JSON.parse(window.atob(token.split('.')[1]));

      if (decodedJWT.role == "admin") {
        this.globals.isSuperAdmin = false;
        this.globals.isAdmin = true;
        this.globals.isUser = false;
        return true;
      }
      else if (decodedJWT.role == "super-admin") {
        this.globals.isSuperAdmin = true;
        this.globals.isAdmin = true;
        this.globals.isUser = false;
        return true;
      }
      else {
        this.globals.isSuperAdmin = false;
        this.globals.isAdmin = false;
        this.globals.isUser = true;
        this.router.navigate(['/login']);
        return false;
      }
    }
    else {
      this.globals.isUser = false;
      this.globals.isAdmin = false;
      this.router.navigate(['/login']);
      return false;
    }
  }

}
