import { Injectable } from '@angular/core';
import { Globals } from '../common/globals';
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(public globals: Globals, private jwtHelper: JwtHelperService) { }

  loggedIn() {
    
    const token = localStorage.getItem('token');

    if (token && !this.jwtHelper.isTokenExpired(token)) {
      this.globals.isLogged = true;
      return true;
    }
    else {
      this.globals.isLogged = false;
      localStorage.removeItem('token');
      return false;
    }
  }
}
