import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { InterceptorSkipHeader } from 'src/app/configuration/http-config-interceptor.service';
import { Observable } from 'rxjs';
import { Employee } from '../models/Employee';

@Injectable({
  providedIn: 'root'
})
export class ModalsService {

  headers = new HttpHeaders().set(InterceptorSkipHeader, '');

  constructor(private http: HttpClient) { }

  changeRole(id: number, role: number): Observable<Employee> {
    const headers = new HttpHeaders().set(InterceptorSkipHeader, '');
    return this.http.put<Employee>('api/admin/change/role/' + id.toString(), role, { headers });
  }

  changeFurlough(id: number, furlough: number): Observable<Employee> {
    const headers = new HttpHeaders().set(InterceptorSkipHeader, '');
    return this.http.put<Employee>('api/admin/change/furlough/' + id.toString(), furlough, { headers });
  }
}