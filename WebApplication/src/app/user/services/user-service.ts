import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Role } from '../models/Role';
import { LoginDto } from '../models/dtos/LoginDto';
import { Employee } from '../models/Employee';
import { EmployeeDto } from '../models/dtos/EmployeeDto';
import { InterceptorSkipHeader } from 'src/app/configuration/http-config-interceptor.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  headers = new HttpHeaders().set(InterceptorSkipHeader, '');

  constructor(private http: HttpClient) { }

  addRole(info: Role): Observable<void> {
    return this.http.post<void>('/api/role/create', info);
  }

  register(info: Employee): Observable<Employee> {
    const headers = new HttpHeaders().set(InterceptorSkipHeader, '');
    return this.http.post<Employee>('api/register/employee', info, { headers });
  }

  login(info: LoginDto): Observable<any> {
    const headers = new HttpHeaders().set(InterceptorSkipHeader, '');
    return this.http.post<any>('/api/login', info, { headers });
  }

  getEmployee(id: number): Observable<EmployeeDto> {
    return this.http.get<EmployeeDto>('/api/employee/' + id.toString())
  }
}