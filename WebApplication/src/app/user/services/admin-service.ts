import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { InterceptorSkipHeader } from 'src/app/configuration/http-config-interceptor.service';
import { EmployeeSearchDto } from '../models/dtos/EmployeeSearchDto';
import { Observable } from 'rxjs';
import { Employee } from '../models/Employee';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  readonly url = "/api/admin";
  headers = new HttpHeaders().set(InterceptorSkipHeader, '');

  constructor(private http: HttpClient) { }

  searchUsers(filter: EmployeeSearchDto): Observable<Employee[]> {
    let query = this.url + "/users" + this.composeQueryString(filter);
    return this.http.get<Employee[]>(query);
  }

  getMaxFurloughLeft(): Observable<number>{
    return this.http.get<number>(this.url + "/users/maxFurlough");
  }

  composeQueryString(object: any): string {
    let result = '';
    let isFirst = true;
    if (object) {
      Object.keys(object)
        .filter(key => object[key] !== null && object[key] !== undefined)
        .forEach(key => {
          let value = object[key];
          if (value instanceof Date) {
            value = value.toISOString();
          }
          if (isFirst) {
            result = '?' + key + '=' + value;
            isFirst = false;
          } else {
            result += '&' + key + '=' + value;
          }
        });
    }
    return result;
  }
}