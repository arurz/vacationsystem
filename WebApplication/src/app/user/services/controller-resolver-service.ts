import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { AdminService } from './admin-service';
import { EmployeeSearchDto } from '../models/dtos/EmployeeSearchDto';

@Injectable({
  providedIn: 'root'
})
export class ControllerResolverService implements Resolve<any> {
  constructor(private adminService: AdminService) { }
  resolve(route: ActivatedRouteSnapshot) {

    let filter: EmployeeSearchDto = new EmployeeSearchDto();
    filter.role = -1;
    filter.leastDaysFurloughLeft = 0;
    filter.mostDaysFurloughLeft = 0;

    return this.adminService.searchUsers(filter);
  }
}