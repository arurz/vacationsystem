import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { UserService } from './user-service';

@Injectable({
  providedIn: 'root'
})
export class ProfileResolverService implements Resolve<any> {
  constructor(private userService: UserService) { }
  resolve(route: ActivatedRouteSnapshot) {
    return this.userService.getEmployee(+route.params['id']);
  }
}