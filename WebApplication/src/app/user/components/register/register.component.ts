import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user-service';
import { Employee } from '../../models/Employee';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/common/services/notification.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  employee: Employee = new Employee();

  constructor(private userService: UserService,
    private router: Router,
    private notificationService: NotificationService) { }

  ngOnInit(): void {
  }

  submit(): void {
    this.userService.register(this.employee)
      .subscribe(() => {
        this.notificationService.showSuccess("Registration succeeded", "Vacation System");
        this.router.navigateByUrl('login')
      },(err: any) => {
        console.log(err);
        this.notificationService.showError(err.error.Message, err.error.ClassName);
      });
  }
}
