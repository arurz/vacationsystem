import { Component, OnInit } from '@angular/core';
import { EmployeeDto } from '../../models/dtos/EmployeeDto';
import { Status } from 'src/app/request/models/enums/Status';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Globals } from 'src/app/common/globals';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ModalsService } from '../../services/modals-service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  employeeDto: EmployeeDto = new EmployeeDto();
  adminOwnProfile: boolean = false;

  title = 'appBootstrap';
  closeResult: string;

  p: number = 1;
  count: number = 7;

  newRole: number = 0;
  newFurlough: number = 0;

  constructor(private route: ActivatedRoute,
    private spinnerService: NgxSpinnerService,
    public globals: Globals,
    private modalService: NgbModal,
    private modalsService: ModalsService) { }

  ngOnInit(): void {
    this.spinnerService.show();
    this.getEmployee();
    this.setDefaultRole();
    this.setDefaultFurlough();
  }

  setDefaultRole(){
    if(this.employeeDto.employee.role.id == 2){
        this.newRole = 2;
    }
    else if(this.employeeDto.employee.role.id == 1){
        this.newRole = 1;
    }
  }

  setDefaultFurlough(){
    this.newFurlough = this.employeeDto.employee.furloughLeft;
  }

  getEmployee(): void {
    this.route.data.subscribe((response: any) => {
      this.employeeDto = response.user;
      this.convertDatesAndStatus();
      this.checkIfAdminOwnProfile();
    })
  }

  convertDatesAndStatus(): void {
    this.employeeDto.requests.forEach(element => {
      element.statusString = Status[element.status];
    });
    this.spinnerService.hide();
  }

  open(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
    } else {
        return `with: ${reason}`;
    }
  }

  changeRole() {
    this.modalsService.changeRole(this.employeeDto.employee.id, this.newRole)
      .subscribe(employee => {
        this.employeeDto.employee = employee;
        location.reload();
      });
  }
  changeFurlough() {
    this.modalsService.changeFurlough(this.employeeDto.employee.id, this.newFurlough)
      .subscribe(employee => {
        this.employeeDto.employee = employee;
        location.reload();
      });
  }

  checkIfAdminOwnProfile() {
    const token: any = localStorage.getItem("token");
    let decodedJWT = JSON.parse(window.atob(token.split('.')[1]));
    let id = Number(decodedJWT.userId);
    if (id === this.employeeDto.employee.id) {
        this.adminOwnProfile = true;
    }
  }
}
