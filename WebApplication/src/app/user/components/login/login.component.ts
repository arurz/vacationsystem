import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginDto } from '../../models/dtos/LoginDto';
import { UserService } from '../../services/user-service';
import { Globals } from 'src/app/common/globals';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from 'src/app/common/services/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginDto: LoginDto = new LoginDto();

  constructor(private userService: UserService,
    private router: Router,
    private globals: Globals,
    private spinnerService: NgxSpinnerService,
    private notificationService: NotificationService) { }

  ngOnInit(): void {
  }

  submit(): void {
    this.userService.login(this.loginDto)
      .subscribe(
        (res: any) => {
          localStorage.setItem('token', res.token);
          let decodedJWT = JSON.parse(window.atob(res.token.split('.')[1]));

          if (decodedJWT.role == "admin") {
              this.globals.isAdmin = true;
              this.router.navigateByUrl('search');
          }
          else if (decodedJWT.role == "super-admin") {
              this.spinnerService.hide();
              this.globals.isSuperAdmin = true;
              this.globals.isAdmin = true;
              this.router.navigateByUrl('search');
          }
          else {
              this.globals.isUser = true;
              this.router.navigateByUrl('request/creation');
          }
          console.log(res);
          this.notificationService.showSuccess("You are logged in", "Vacation System");
          this.globals.isLogged = true;
        },error => {
          console.log(error);
          this.notificationService.showError(error.error.Message, error.error.ClassName);
        });
  }
}
