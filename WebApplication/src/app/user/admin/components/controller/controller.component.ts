import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/user/models/Employee';
import { EmployeeSearchDto } from 'src/app/user/models/dtos/EmployeeSearchDto';
import { Options } from '@angular-slider/ngx-slider';
import { AdminService } from 'src/app/user/services/admin-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { Globals } from 'src/app/common/globals';

@Component({
  selector: 'app-controller',
  templateUrl: './controller.component.html',
  styleUrls: ['./controller.component.css']
})
export class ControllerComponent implements OnInit {

  maxFurloughLeft: number;
  employees: Employee[] = [];
  filter: EmployeeSearchDto = new EmployeeSearchDto();

  p: number = 1;
  count: number = 7;

  minValue: number = 0;
  maxValue: number = 0;
  options: Options = { ceil: 10000 };

  constructor(private adminService: AdminService,
    private spinnerService: NgxSpinnerService,
    private route: ActivatedRoute,
    private globals: Globals) { }

  ngOnInit(): void {
    this.globals.isFirstLoad = true;
    this.filter.role = -1;
    this.search();
  }

  setOptions(): void {
    let opts: Options = {
      floor: 0,
      step: 1,
      ceil: this.maxFurloughLeft,
      showTicks: true
    };
    this.options = opts;
  }

  getMaxFurloughLeft(): void {
    this.maxFurloughLeft = this.employees[0].furloughLeft;
    for (let i = 0; i < this.employees.length; i++) {
      if (this.employees[i].furloughLeft > this.maxFurloughLeft) {
          this.maxFurloughLeft = this.employees[i].furloughLeft;
      }
    }
    this.setOptions();
  }

  search(): void {
    this.spinnerService.show();
    
    if(this.globals.isFirstLoad == true){
        this.route.data.subscribe((response: any) => {
        this.employees = response.users;
        this.getMaxFurloughLeft();
        this.globals.isFirstLoad = false;
        this.spinnerService.hide();
      });
    }
    else{
      this.filter.leastDaysFurloughLeft = this.minValue;
      this.filter.mostDaysFurloughLeft = this.maxValue;

      this.adminService.searchUsers(this.filter)
        .subscribe(res => {
            this.employees = res;
            this.spinnerService.hide();
          } 
        )
    }
  }

  deleteFilter() {
    this.maxValue = 0;
    this.minValue = 0;
    this.filter = new EmployeeSearchDto();
    this.filter.role = -1;

    this.search();
  }
}
