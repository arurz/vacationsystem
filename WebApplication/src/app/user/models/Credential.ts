export class Credential {
  id: number;
  employeeId: number;
  password: string;
  repeatPassword: string;
}