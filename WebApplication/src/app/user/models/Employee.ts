import { Role } from './Role';
import { Credential } from './Credential';

export class Employee {
  id: number;
  firstname: string;
  middlename: string;
  lastname: string;
  username: string;
  email: string;
  
  position: string;
  furloughLeft: number;
  
  role: Role;
  credential: Credential = new Credential();
}