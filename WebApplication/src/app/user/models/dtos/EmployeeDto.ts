import { RequestModel } from '../../../request/models/request.model'
import { Employee } from '../Employee';

export class EmployeeDto {
  employee: Employee;
  requests: RequestModel[];
}