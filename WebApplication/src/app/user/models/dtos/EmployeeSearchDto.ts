export class EmployeeSearchDto {
  firstname: string;
  middlename: string;
  lastname: string;
  username: string;
  email: string;

  role: number;
  position: string;

  mostDaysFurloughLeft: number;
  leastDaysFurloughLeft: number;
}