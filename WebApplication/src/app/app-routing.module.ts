import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RequestCreationComponent } from './request/components/request-creation/request-creation.component';
import { RegisterComponent } from './user/components/register/register.component';
import { LoginComponent } from './user/components/login/login.component';
import { HomeComponent } from './menu/home/home.component';
import { SearchComponent } from './menu/search/components/search/search.component';
import { ControllerComponent } from './user/admin/components/controller/controller.component';
import { AuthGuard } from './authentication/employee-auth.guard';
import { AdminAuthGuard } from './authentication/admin-auth.guard';
import { ProfileComponent } from './user/components/profile/profile.component';
import { RequestViewComponent } from './request/components/request-view/request-view.component';
import { RequestEditingComponent } from './request/components/request-editing/request-editing.component';
import { RequestResolverService } from './request/services/request-resolver.service';
import { ProfileResolverService } from './user/services/profile-resolver-service';
import { ControllerResolverService } from './user/services/controller-resolver-service';
import { SearchResolverService } from './menu/search/services/search-resolver.service';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },

  { path: 'admin', component: ControllerComponent, canActivate: [AdminAuthGuard], resolve: { users: ControllerResolverService } },
  { path: 'search', component: SearchComponent, canActivate: [AdminAuthGuard], resolve: { requests: SearchResolverService } },

  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'profile/:id', component: ProfileComponent, canActivate: [AuthGuard], resolve: { user: ProfileResolverService } },

  { path: 'request/editing/:id', component: RequestEditingComponent, canActivate: [AuthGuard] },
  { path: 'request/creation', component: RequestCreationComponent, canActivate: [AuthGuard] },
  { path: 'request/:id', component: RequestViewComponent, canActivate: [AuthGuard], resolve: { products: RequestResolverService } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
