import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DatePipe } from '@angular/common';

import { RequestEditingComponent } from './request/components/request-editing/request-editing.component';
import { RequestCreationComponent } from './request/components/request-creation/request-creation.component';
import { RequestViewComponent } from './request/components/request-view/request-view.component';

import { LoginComponent } from './user/components/login/login.component';
import { RegisterComponent } from './user/components/register/register.component';

import { HomeComponent } from './menu/home/home.component';
import { MenuComponent } from './menu/menu-component/menu.component';
import { SearchComponent } from './menu/search/components/search/search.component';
import { ControllerComponent } from './user/admin/components/controller/controller.component';

import { ProfileComponent } from './user/components/profile/profile.component';

import { Globals } from './common/globals';
import { JwtModule } from '@auth0/angular-jwt';

import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { NgxSpinnerModule } from "ngx-spinner";
import { ToastrModule } from 'ngx-toastr';

export function tokenGetter() {
  return localStorage.getItem("token");
}
import { HttpConfigInterceptorService } from './configuration/http-config-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    MenuComponent,
    RequestCreationComponent,
    HomeComponent,
    LoginComponent,
    SearchComponent,
    ControllerComponent,
    RequestViewComponent,
    ProfileComponent,
    RequestEditingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    NgxSliderModule,
    NgbModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ["localhost:1508"],
        disallowedRoutes: []
      }
    })
  ],
  providers: [
    DatePipe,
    Globals,
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
